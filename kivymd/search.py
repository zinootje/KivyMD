# -*- coding: utf-8 -*-
"""
Search Patterns
===============

`Material Design spec Search Patterns page <https://www.google.com/design/spec/patterns/search.html>`

KivyMD currently only provides the Persistent Search pattern via the :class:`MDPersistentSearch:` widget.

Persistent Search Example
-------------------------

.. note::

    This widget is designed to be called from Python code only.

.. code-block:: python

"""
from kivy.lang import Builder
from kivy.metrics import sp, dp
from kivy.properties import NumericProperty, ListProperty, ObjectProperty, \
    StringProperty
from kivy.uix.modalview import ModalView
from kivy.uix.textinput import TextInput
from kivy.animation import Animation
from kivy.core.window import Window
from kivymd import images_path
from kivymd.label import MDLabel
from kivymd.theming import ThemableBehavior
from kivymd.list import TwoLineAvatarIconListItem

Builder.load_string("""
#:import MDList kivymd.list.MDList
#:import MDCard kivymd.card.MDCard
#:import Toolbar kivymd.toolbar.Toolbar
<MDPersistentSearch>
    search_input: search_input
    main_bl: main_bl
    canvas:
        Color:
            rgba: root._current_bg_color
        Rectangle:
            size: self.size
            pos: self.pos
    FloatLayout:
        BoxLayout:
            orientation: 'vertical'
            id: main_bl
            size_hint: None, None
            height: Window.height - dp(8*3+48)
            width: Window.width - dp(8) * 2
            top: Window.height - dp(8*2+48)
            x: dp(8)
            MDCard:
                id: main_card
                opacity: 0
                ScrollView:
                    MDList:
                        id: main_ml
        MDCard:
            id: search_box
            opacity: 0
            size_hint: None, None
            height: dp(48)
            width: Window.width - dp(8) * 2
            top: Window.height + dp(48)
            # top: Window.height - dp(8)
            x: dp(8)
            orientation: 'horizontal'
            MDIconButton:
                icon: 'arrow-left'# if search_input.focus else 'search'
                theme_text_color: 'Secondary'
                on_release: root.dismiss()# if search_input.focus else None  # FIXME
            BoxLayout:
                padding: dp(5), dp(4), 0, 0  # FIXME: NOT EXACT METRIC
                SearchTextInput:
                    id: search_input
                    hint_text: "Search"  # TODO: i18n
                    theme_text_color: 'Primary'
                    on_text_validate: root.search()
            MDIconButton:
                # icon: 'mic' if search_input.text == '' else 'close'
                icon: 'close'
                theme_text_color: 'Secondary'
                # on_release: root.mic_input() if search_input.text == '' else \
                #     root.clear_input()
                on_release: root.clear_input()

<SearchTextInput>
    canvas.before:
        Clear
        Color:
            rgba: (self.cursor_color if self.focus and not self.cursor_blink \
            else (0, 0, 0, 0))
        Rectangle:
            pos: [int(x) for x in self.cursor_pos]
            size: 1, -self.line_height
        Rectangle:
            texture: self._hint_lbl.texture
            size: self._hint_lbl.texture_size
            pos: self.x, self.y + self._hint_y
        Color:
            rgba: self.disabled_foreground_color if self.disabled else \
            (self.hint_text_color if not self.text and not self.focus else \
            self.foreground_color)
    font_name: 'Roboto'
    font_size: sp(20)
    multiline: False
    cursor_color: root.theme_cls.secondary_text_color
    foreground_color: root.theme_cls.text_color
    MDLabel:
        id: _hint_lbl
        font_style: 'Caption'
        theme_text_color: 'Custom'
        text_color: root._hint_txt_color if not root.text and not root.focus \
        else ((1, 1, 1, 0) if not root.text or root.focus \
        else (1, 1, 1, 0))
<SearchItem>
    on_release: exec(self.callback)
""")


class SearchResult:
    def __init__(self, title="", info="", callback="", metadata=""):
        '''
        An item that will show up in a search
        :param title: Displayed at the top of the list item
        :param info: The secondary text of the list item
        :param callback: The function to be called on click
        :param metadata: Other info
        '''
        self.title = title
        self.info = info
        self.callback = callback
        self.meta = metadata


class SearchResultWidget(TwoLineAvatarIconListItem):
    def __init__(self, callback, index, search_cls, **kwargs):
        """
        :type callback: function
        :type index: int
        :type search_cls: MDPersistentSearch / your own search modal
        """
        self.callback = callback
        super(TwoLineAvatarIconListItem, self).__init__(**kwargs)
        self.ids._lbl_primary.markup = True
        self.index = index
        self.search_cls = search_cls
        self.item_text = str(self.text)
        self.ids._lbl_primary.text = '{}[b]{}[/b]{}'\
            .format(self.item_text[0:self.index],
                    self.item_text[self.index:len(self.search_cls.current_search)+self.index],
                    self.item_text[self.index+len(self.search_cls.current_search):len(self.item_text)]
                    )

    def on_release(self):
        self.search_cls.dismiss()
        self.dispatch('on_press')


class SearchTextInput(ThemableBehavior, TextInput):
    _hint_txt_color = ListProperty()
    _hint_lbl = ObjectProperty()
    _hint_lbl_font_size = NumericProperty(sp(16))
    _hint_y = NumericProperty(dp(10))
    master = ObjectProperty()

    def __init__(self, **kwargs):
        self._hint_lbl = MDLabel(font_style='Subhead',
                                 halign='left',
                                 valign='middle')
        super(SearchTextInput, self).__init__(**kwargs)
        self.bind(text=self.on_text)

    def on_text(self, instance, value):
        """
        :type value: str
        Refreshes the search whenever you type
        """
        if self.master is not None:
            self.master.refresh_search_results(value)

    def on_master(self, instance, master):
        """
        :type master: MDPersistentSearch
        Sets the parent search modal
        """
        if self.master is None:
            self.master = master


class SearchBase(object):
    """
    Important: When writing your own search widget, you must have a MDList with the id "main_ml" (meaning "main material design list")
    That list is where SearchBase puts all of the search results.
    If you don't wish to do this, you can just inherit from SearchBase, and change all of the references to "self.ids.main_ml"
    """
    def __init__(self, results):
        """
        :type results: list
        results is a list containing SearchResult objects
        """
        self.all_results = results
        self.current_search = ""

    def refresh_search_results(self, search):
        """
        :type search: str
        """
        self.current_search = search
        self.ids.main_ml.clear_widgets()
        search_old = search
        search = search.lower()
        found_any = False
        if len(search) == 0:
            for result in self.all_results:
                self.ids.main_ml.add_widget(SearchResultWidget(text=result.title,
                                                               secondary_text=result.info,
                                                               index=len(result.title) + 1,
                                                               callback=result.callback,
                                                               search_cls=self))
        else:
            for result in self.all_results:
                if search in result.title.lower():
                    for char, index in zip(result.title.lower(), range(len(result.title.lower()))):
                        if char == search[0]:
                            if result.title.lower()[index:index + len(search)] == search:
                                if not found_any:
                                    found_any = True
                                item = SearchResultWidget(text=result.title,
                                                          secondary_text=result.info,
                                                          index=index,
                                                          callback=result.callback,
                                                          search_cls=self)
                                self.ids.main_ml.add_widget(item)
                                break

        if not found_any:
            self.ids.main_ml.add_widget(MDLabel(text="No results found for \"{}\"".format(search_old),
                                                theme_text_color='Primary',
                                                halign='center'))

    def mic_input(self):
        # TODO: Implement mic_input via Plyer.
        raise NotImplementedError()


class MDPersistentSearch(ThemableBehavior, ModalView, SearchBase):
    main_bl = ObjectProperty()
    search_input = ObjectProperty()
    background_color = ListProperty([0, 0, 0, 0])
    background = "{}transparent.png".format(images_path)

    _hint_text = StringProperty()
    _current_bg_color = ListProperty([0, 0, 0, 0])

    def __init__(self, results):
        super(MDPersistentSearch, self).__init__(results=results)
        self.ids.search_input.master = self
        self.close_anims_done = 0

    def search(self):
        self.refresh_search_results(self.search_input.text)

    def clear_input(self):
        self.search_input.text = ""

    def animation_done(self, *args):
        # I really don't like this system, it needs re-doing
        self.close_anims_done += 1
        if self.close_anims_done >= 3:  # 3 = Number of animations in self.dismiss
            super(MDPersistentSearch, self).dismiss()

    def open(self, *args):
        super(MDPersistentSearch, self).open(args)
        Animation(duration=.25, _current_bg_color=self.theme_cls.bg_darkest).start(self)

        Animation(duration=.3, top=Window.height - dp(8), opacity=1, t='out_quad').start(self.ids.search_box)

        anim_main_body = Animation(duration=.3, top=Window.height - dp(8*2+48), t='out_quad')
        anim_main_body &= Animation(duration=.3, opacity=1)
        anim_main_body.start(self.ids.main_card)

    def dismiss(self, *args, **kwargs):
        anim_bg = Animation(duration=.25, _current_bg_color=(0, 0, 0, 0))
        anim_bg.bind(on_complete=self.animation_done)
        anim_bg.start(self)

        anim_search = Animation(duration=.3, top=Window.height + dp(48), opacity=0, t='out_quad')
        anim_search.bind(on_complete=self.animation_done)
        anim_search.start(self.ids.search_box)

        anim_body = Animation(duration=.3, opacity=0, top=0, t='out_quad')
        anim_body.bind(on_complete=self.animation_done)
        anim_body.start(self.ids.main_card)
