from __future__ import print_function  # P4A is often using PY2, and I debug using the print function :P

import kivy
from kivy.app import App
from kivy.core.window import Window
from kivy.event import EventDispatcher
from kivy.logger import Logger
from kivy.properties import ObjectProperty, NumericProperty, OptionProperty
from kivy.setupconfig import USE_SDL2

from kivymd.color_definitions import colors
from kivymd.platforms.android_device import autoclass, JavaException, cast
from kivymd.platforms.android_device.utils import hex_to_jcolor
from kivymd.platforms.decorators import android_api_required, platform_required, run_on_ui_thread
from kivymd.theming import ThemeManager


class AndroidDevice(EventDispatcher):  # EventDispatcher so status_bar_height is bound
    status_bar_height = NumericProperty(0)
    android_device_type = OptionProperty('standard', options=['standard', 'tv'])

    def __init__(self, sdk_ver):
        self.sdk_ver = sdk_ver


class DefaultAppConfig(EventDispatcher):
    # You should inherit from this class and then override the things you want to change
    # Then set your app's 'app_config_cls' to your inherited class (not an instance of it!!)
    android_use_status_bar_color = False  # Not to be used with draw_own_status_bar
    android_use_navigation_bar_color = False  # Not to be used with draw_own_status_bar
    android_draw_own_status_bar = True  # KivyMD will draw the status bar color
    android_wm_flags = []
    android_decor_view_flags = []
    android_hide_status_bar = False  # Equivalent to 'fullscreen = 1' in buildozer.spec
    android_hide_navigation_bar = False
    android_immersive = False  # https://developer.android.com/reference/android/view/View.html#SYSTEM_UI_FLAG_IMMERSIVE
    android_immersive_sticky = True  # https://developer.android.com/reference/android/view/View.html#SYSTEM_UI_FLAG_IMMERSIVE_STICKY


class MDApp(App):
    theme_cls = ThemeManager()
    _android_activity = ObjectProperty(None, allow_none=True)
    _android_service = ObjectProperty(None, allow_none=True)

    _android_current_intent = ObjectProperty(None)

    _app_config = ObjectProperty(None)
    _device = ObjectProperty(None, allow_none=True)

    app_config_cls = DefaultAppConfig

    def __init__(self, **kwargs):
        super(MDApp, self).__init__(**kwargs)

    @platform_required('android')
    @run_on_ui_thread
    def _android_window_flag_operation(self, flag, operation):
        # List of flags: https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html
        wm_layout_params = autoclass('android.view.WindowManager$LayoutParams')
        window = self._android_activity.getWindow()
        flag = getattr(wm_layout_params, flag)
        if operation == "remove":
            window.clearFlags(flag)
        elif operation == "add":
            window.addFlags(flag)

    @platform_required('android')
    def _android_set_window_flag(self, flag):
        # List of flags: https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html
        self._android_window_flag_operation(flag=flag, operation="add")

    @platform_required('android')
    def _android_clear_window_flag(self, flag):
        # List of flags: https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html
        self._android_window_flag_operation(flag=flag, operation="remove")

    @platform_required('android')
    @android_api_required(21)
    @run_on_ui_thread
    def _android_update_activity_color(self, *args):
        Logger.debug("MDApp: Updating android activity color")
        new_color = hex_to_jcolor(colors[self.theme_cls.primary_palette][self.theme_cls.primary_hue])
        task_description = autoclass('android.app.ActivityManager$TaskDescription')
        desc = task_description(task_description().getLabel(), task_description().getIcon(), new_color)
        # We have to do some weird translation of colors, since we are talking to Java here
        # For example, we invert the color, then use the negative int value of the color (practically inverting again)
        # If we don't do this, the ActivityManager says the color is not 100% opaque (just because of language diffs)
        self._android_activity.mActivity.setTaskDescription(desc)

        window = self._android_activity.getWindow()
        new_color = hex_to_jcolor(colors[self.theme_cls.primary_palette]['700'])
        if self._app_config.android_use_status_bar_color and not self._app_config.android_draw_own_status_bar:
            window.setStatusBarColor(new_color)
        elif self._app_config.android_use_status_bar_color and self._app_config.android_draw_own_status_bar:
            Logger.debug("MDApp: Skipping setting status bar color, since we drawing it ourselves")

    @platform_required('android')
    @run_on_ui_thread
    def android_send_toast(self, text, length_long=False, x_offset=None, y_offset=None, gravity=None):
        # Gravity and offset values would have defaults, but I haven't got a slightest clue how they work
        android_toast = autoclass('android.widget.Toast')
        context = self._android_activity.mActivity
        duration = android_toast.LENGTH_LONG if length_long else android_toast.LENGTH_SHORT
        j_string = autoclass('java.lang.String')
        c = cast('java.lang.CharSequence', j_string(text))
        t = android_toast.makeText(context, c, duration)
        t.setGravity(gravity or t.getGravity(), x_offset or t.getXOffset(), y_offset or t.getYOffset())
        t.show()

    @platform_required('android')
    @run_on_ui_thread
    def _setup_android(self, *args):
        Window.softinput_mode = 'below_target'
        try:
            sdk_ver = autoclass('android.os.Build$VERSION').SDK_INT
        except JavaException as e:
            Logger.warn("MDApp: " + e)
            Logger.warn("MDApp: Unable to determine SDK version of android device, presuming the device is running "
                        "Android Froyo or older")
            if autoclass('java.lang.System').getProperty('java.vm.version')[0] == 2:
                Logger.critical("MDApp: System using ART runtime, and a JNI exception has occurred, the app will "
                                "now crash, no matter what you do")  # The app usually dies before this exception
                # This is an edge case, since the SDK version should found if the OS is new enough to use ART
            sdk_ver = 1
            # android.os.Build.VERSION was added in SDK 4 (Froyo), I hope anyone who gets this message is seriously
            # considering getting a new device
        self._device = AndroidDevice(sdk_ver=sdk_ver)
        if USE_SDL2:
            self._android_activity = autoclass('org.kivy.android.PythonActivity')
            self._android_service = autoclass('org.kivy.android.PythonService')
        else:
            self._android_activity = autoclass('org.renpy.android.PythonActivity')
            self._android_service = autoclass('org.renpy.android.PythonService')
        conf = self._app_config

        # self._android_activity.bind(on_new_intent=self.on_new_intent)

        if not conf.android_hide_status_bar:
            self._android_clear_window_flag('FLAG_FULLSCREEN')
        # ^ Override Buildozer, if you want fullscreen, see DefaultAppConfig.android_hide_status_bar

        # Draw own status bar:
        resource_id = self._android_activity.getResources().getIdentifier("status_bar_height", "dimen", "android")
        if resource_id > 0:
            status_bar_height = self._android_activity.getResources().getDimensionPixelSize(resource_id)
        else:
            status_bar_height = 0
        self._device.status_bar_height = status_bar_height
        if not conf.android_draw_own_status_bar:
            if self._device.sdk_ver >= 21:
                self._android_set_window_flag(flag='FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS')
            self._device.status_bar_height = 0

        # WM flags
        if conf.android_wm_flags:
            for flag in conf.android_wm_flags:
                self._android_set_window_flag(flag)
        if self._device.sdk_ver >= 21:  # Android L or up
            self._android_update_activity_color()
            self.theme_cls.bind(primary_palette=self._android_update_activity_color)

        # WM decor flags
        decor_flags = 0
        view_java = autoclass('android.view.View')
        decor_view = self._android_activity.getWindow().getDecorView()
        if conf.android_draw_own_status_bar and self._device.sdk_ver >= 21:
            self._android_set_window_flag(flag='FLAG_TRANSLUCENT_STATUS')
            self._android_set_window_flag(flag='FLAG_LAYOUT_IN_SCREEN')
            decor_flags |= view_java.SYSTEM_UI_FLAG_LAYOUT_STABLE
            decor_flags |= view_java.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        elif conf.android_draw_own_status_bar and self._device.sdk_ver < 21:
            Logger.warn("MDApp: draw_own_status_bar is not supported on pre Lollipop devices")

        def _add_decor_flag_from_config(required_config, min_sdk, flag_str):
            if getattr(self._app_config, required_config, False):
                if self._device.sdk_ver >= min_sdk:
                    return getattr(view_java, flag_str)
                else:
                    Logger.warn("MDApp: _app_config.{} requires a minimum of api {} (device using {})".format(
                        required_config, min_sdk, self._device.sdk_ver))
            return 0
        # TODO: test
        decor_flags |= _add_decor_flag_from_config('android_immersive', 19, 'SYSTEM_UI_FLAG_IMMERSIVE')
        decor_flags |= _add_decor_flag_from_config('android_immersive_sticky', 19, 'SYSTEM_UI_FLAG_IMMERSIVE_STICKY')
        decor_flags |= _add_decor_flag_from_config('android_hide_navigation_bar', 16, 'SYSTEM_UI_FLAG_HIDE_NAVIGATION')
        # if conf.android_immersive and self._device.sdk_ver >= 19:
        #     decor_flags |= view_java.SYSTEM_UI_FLAG_IMMERSIVE
        # elif conf.android_immersive and self._device.sdk_ver < 19:
        #     Logger.warn("MDApp: _app_config.{} requires a minimum of api {} (device using {})".format(
        #         "android_immersive", 19, self._device.sdk_ver))

        # if conf.android_immersive_sticky and self._device.sdk_ver >= 19:
        #     decor_flags |= view_java.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        # elif conf.android_immersive_sticky and self._device.sdk_ver < 19:
        #     Logger.warn("MDApp: _app_config.{} requires a minimum of api {} (device using {})".format(
        #         "android_immersive_sticky", 19, self._device.sdk_ver))

        # if conf.android_hide_navigation_bar and self._device.sdk_ver >= 16:
        #     decor_flags |= view_java.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        # elif conf.android_hide_navigation_bar and self._device.sdk_ver < 16:
        #     Logger.warn("MDApp: _app_config.{} requires a minimum of api {} (device using {})".format(
        #         "android_hide_navigation_bar", 16, self._device.sdk_ver))

        for flag in conf.android_decor_view_flags:
            decor_flags |= getattr(view_java, flag, 0)
        decor_view.setSystemUiVisibility(decor_flags)

        # Android TV detection
        package_manager_java = autoclass('android.content.pm.PackageManager')
        if self._android_activity.getPackageManager().hasSystemFeature(package_manager_java.FEATURE_TELEVISION):
            self._device.android_device_type = "tv"

    def on_pause(self):
        return True

    def on_resume(self):
        if kivy.platform == "android":
            # force a check of the current Intent:
            self.on_new_intent(self._android_activity.getIntent())

    def on_start(self):
        if kivy.platform == "android":
            # force a check of the current Intent:
            self.on_new_intent(self._android_activity.getIntent())

    def on_new_intent(self, intent):
        if intent == self._android_current_intent:
            Logger.debug("MDApp: on_new_intent: Previously handled intent.")
            return
        Logger.debug("MDApp: on_new_intent: New intent; save it.")
        self._android_current_intent = intent

        print(intent)

        # TODO: GET EXTRA intent.getStringExtra("id")
        # self._android_activity.setIntent(intent)
        # ....now process the Intent data...

    def run(self):
        self._app_config = self.app_config_cls()
        if kivy.platform == 'android':
            self._setup_android()

        super(MDApp, self).run()

# Notes and things:
# Buildozer:
# fullscreen = 1 required (it wont actually do fullscreen, so don't worry if you want a fullscreen app. If you do want
        # a fullscreen app, see DefaultAppConfig)
