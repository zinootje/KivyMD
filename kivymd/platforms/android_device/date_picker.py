import datetime
from functools import partial

from kivy.event import EventDispatcher
from kivy.properties import ObjectProperty, ListProperty

from kivymd.app import MDApp
from kivymd.platforms.android_device import java_method, autoclass, cast, PythonJavaClass
from kivymd.platforms.android_device.utils import color_to_jcolor
from kivymd.platforms.decorators import platform_required, android_api_required, run_on_ui_thread
from kivymd.theming import ThemableBehavior


class _OnDateChangedListener(PythonJavaClass):
    __javainterfaces__ = ['android/app/DatePickerDialog$OnDateSetListener', ]
    __javacontext__ = 'app'

    @platform_required('android')
    def __init__(self, py_dialog, action):
        self.py_dialog = py_dialog
        self.action = action
        super(_OnDateChangedListener, self).__init__()

    @platform_required('android')
    @java_method('(Landroid/widget/DatePicker;III)V')
    def onDateSet(self, dialog, year, day, month):
        print("BUTTON CLICKED!", dialog, year, day, month)
        dt = datetime.date(day=day, month=month+1, year=year)
        # Month +1 because month is between 0 and 11
        self.py_dialog.dispatch('on_date', dt)
        self.action(dt)


class AndroidDatePickerDialog(EventDispatcher, ThemableBehavior):
    __events__ = ('on_dismiss', 'on_date')
    _date_picker_dialog = ObjectProperty()
    _context = ObjectProperty()
    _color = ListProperty(None)

    @platform_required('android')
    @android_api_required(21)
    def __init__(self, callback, day=None, month=None, year=None):
        super(AndroidDatePickerDialog, self).__init__()
        # Java classes
        self._datepicker_java = autoclass('android.app.DatePickerDialog')
        # date = autoclass('org.kivymd.android.KivyMDDatePickerDialog')

        today = datetime.date.today()

        self.day = day or today.day
        self.month = month or today.month
        self.month -= 1  # Android interprets months between 0 and 11
        self.year = year or today.year
        self.callback = callback

        self._context = MDApp.get_running_app()._android_activity.mActivity

    @platform_required('android')
    @android_api_required(21)
    @run_on_ui_thread
    def open(self):
        r_style = autoclass('android.R$style')
        style = r_style.Theme_Material_Dialog if self.theme_cls.theme_style == "Dark" else \
            r_style.Theme_Material_Light_Dialog

        self._date_picker_dialog = self._datepicker_java(self._context, style,
                                                         _OnDateChangedListener(self, partial(self.callback)),
                                                         self.year, self.month, self.day)
        self._date_picker_dialog.show()

        if self._color is None:
            self._color = color_to_jcolor(self.theme_cls.primary_color)
        else:
            self._color = color_to_jcolor(self._color)

        # Set color:
        # dp_dialog.setColors(self._context, red_col)
        dp = self._date_picker_dialog.getDatePicker()

        root_view = cast('android.view.ViewGroup', dp.getChildAt(0))
        header_view = root_view.getChildAt(0)
        # 5.0
        header_id = self._context.getResources().getIdentifier("day_picker_selector_layout", "id", "android")
        if header_id == header_view.getId():
            header_view.setBackgroundColor(self._color)
        # 6.0+
        header_id = self._context.getResources().getIdentifier("date_picker_header", "id", "android")
        if header_id == header_view.getId():
            header_view.setBackgroundColor(self._color)

        self._date_picker_dialog.getButton(-2).setTextColor(self._color)
        self._date_picker_dialog.getButton(-1).setTextColor(self._color)

    def on_dismiss(self, instance):
        pass

    def on_date(self, date):
        pass

# TODO: Events
# @run_on_ui_thread
# def _test_dp(self, day=25, month=12, year=2016):
#     self._test_dialog()
#     return
#
#     date = autoclass('android.app.DatePickerDialog')
#     # date = autoclass('org.kivymd.android.KivyMDDatePickerDialog')
#     # AlertDialog = autoclass('android.app.AlertDialog')
#     # view_group = autoclass('android.view.ViewGroup')
#     r_style = autoclass('android.R$style')
#
#     red_col = hex_to_jcolor(colors[self.theme_cls.primary_palette][self.theme_cls.primary_hue])
#
#     context = self._android_activity.mActivity
#     month -= 1  # Android interprets months between 0 and 11
#     style = r_style.Theme_Material_Dialog if self.theme_cls.theme_style == "Dark" else \
#         r_style.Theme_Material_Light_Dialog
#     dp_dialog = date(context, style, _OnDateChangedListener(partial(self._dp_callback)), year, month, day)
#     dp_dialog.show()
#     # dp_dialog.setColors(context, red_col)
#
#     dp = dp_dialog.getDatePicker()
#
#     print("set header start")
#     root_view = cast('android.view.ViewGroup', dp.getChildAt(0))
#     header_view = root_view.getChildAt(0)
#     # 5.0
#     header_id = context.getResources().getIdentifier("day_picker_selector_layout", "id", "android")
#     if header_id == header_view.getId():
#         header_view.setBackgroundColor(red_col)
#     # 6.0+
#     header_id = context.getResources().getIdentifier("date_picker_header", "id", "android")
#     if header_id == header_view.getId():
#         header_view.setBackgroundColor(red_col)
#     print("set header complete")
#
#     print("set buttons start")
#     dp_dialog.getButton(-2).setTextColor(red_col)
#     dp_dialog.getButton(-1).setTextColor(red_col)
#     print("set buttons complete")
#
#     # dp.mDelegate
