from kivy import platform

if platform == "android":
    from jnius import autoclass, cast, JavaException, PythonJavaClass, java_method, cast
else:

    def java_method(*args, **kwargs):
        pass

    def autoclass(*args, **kwargs):
        pass

    def cast(*args, **kwargs):
        pass

    class PythonJavaClass(object):
        pass

    class JavaException(Exception):
        pass

del platform  # This would break the platform package in kv lang ("import platform")
