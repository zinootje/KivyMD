import sys
import string
from kivy.utils import get_hex_from_color


def hex_to_jcolor(color):
    if sys.version_info.major == 2:
        str_cls = string
    else:
        str_cls = str
    table = str_cls.maketrans('0123456789abcdef', 'fedcba9876543210')
    new_color = color.lower().translate(table).upper()
    return -int(new_color, 16)


def color_to_jcolor(color):
    return hex_to_jcolor(get_hex_from_color(color))
