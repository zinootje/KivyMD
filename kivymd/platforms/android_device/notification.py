# raise NotImplementedError()
# TODO: put extra intent.putExtra("id", user.getUserAccountId()+"")
from kivy.app import App
from kivy.event import EventDispatcher
from kivy.properties import StringProperty, ListProperty, BooleanProperty, NumericProperty, Logger, ObjectProperty

from kivymd.color_definitions import colors
from kivymd.platforms.android_device import autoclass
from kivymd.platforms.android_device.utils import hex_to_jcolor
from kivymd.platforms.decorators import platform_required
from kivymd.theming import ThemableBehavior


class NotificationManager(EventDispatcher):
    current_id = NumericProperty(0)
    notifications = ListProperty()


class AndroidNotification(EventDispatcher, ThemableBehavior):
    title = StringProperty("")
    message = StringProperty("")
    icon = StringProperty("stat_sys_download")
    buttons = ListProperty()
    play_sound = BooleanProperty(False)
    vibrate = BooleanProperty(False)
    pulse_light = BooleanProperty(False)
    persistent = BooleanProperty(False)
    icon_r_cls = StringProperty("android.R$drawable")

    progress_max = NumericProperty(0)
    progress = NumericProperty(0)
    progress_intermediate = BooleanProperty(False)

    update_on_theme_color = BooleanProperty(True)

    _id = NumericProperty(-1)
    _notification = ObjectProperty()

    @platform_required('android')
    def __init__(self, _id=None):
        super(AndroidNotification, self).__init__()
        if _id is None:
            notification_manager.current_id += 1
            self._id = notification_manager.current_id
        else:
            self._id = _id
        notification_manager.notifications.append(self)

        self._android_activity = App.get_running_app()._android_activity
        self._device = App.get_running_app()._device

    @platform_required('android')
    def send(self):
        # We use int() here so that unique id is not changed when we do += 1
        pending_intent = autoclass('android.app.PendingIntent')
        intent_cls = autoclass('android.content.Intent')
        j_string = autoclass('java.lang.String')
        notification_builder = autoclass('android.app.Notification$Builder')
        notification_cls = autoclass('android.app.Notification')
        drawable = autoclass(self.icon_r_cls)
        context = autoclass('android.content.Context')
        system = autoclass('java.lang.System')
        if self._device.sdk_ver >= 23:
            action_builder = autoclass('android.app.Notification$Action$Builder')
        else:
            action_builder = None
        try:
            request_id = 1
            # request_id = 100
            # get a requestid for pending indent
        except Exception as e:
             raise Exception('Unable to get id', str(e))
        service = self._android_activity.mActivity
        intent = intent_cls(self._android_activity.mActivity, self._android_activity.getClass())
        intent.setFlags(intent_cls.FLAG_ACTIVITY_CLEAR_TOP | intent_cls.FLAG_ACTIVITY_SINGLE_TOP |
                        intent_cls.FLAG_ACTIVITY_REORDER_TO_FRONT)
        try:
            icon = getattr(drawable, self.icon)
        except AttributeError:
            Logger.warn("Icon \"{}\" was not found in drawable class \"{}\", using backup icon: " +
                        "android.R$drawable.stat_sys_warning".format(self.icon, self.icon_r_cls))
            drawable = autoclass('android.R$drawable')
            icon = drawable.stat_sys_warning
        noti = notification_builder(self._android_activity.mActivity)
        noti_flags = list()
        if self.play_sound:
            noti_flags.append(notification_cls.DEFAULT_SOUND)
        if self.vibrate:
            noti_flags.append(notification_cls.DEFAULT_VIBRATE)
        if self.pulse_light:
            noti_flags.append(notification_cls.DEFAULT_LIGHTS)
        if self.persistent:
            noti_flags.append(notification_cls.FLAG_ONGOING_EVENT)
            noti_flags.append(notification_cls.FLAG_NO_CLEAR)
            noti.setOngoing(True)
        else:
            noti.setAutoCancel(True)
        noti_flags_int = 0
        for flag in noti_flags:
            noti_flags_int |= flag
        noti.setDefaults(noti_flags_int)
        noti.setContentIntent(pending_intent.getActivity(service, 0, intent, pending_intent.FLAG_ONE_SHOT))
        noti.setContentTitle(j_string(self.title.encode('utf-8')))
        noti.setContentText(j_string(self.message.encode('utf-8')))
        noti.setOnlyAlertOnce(True)
        noti.setProgress(self.progress_max, self.progress, self.progress_intermediate)
        # https://developer.android.com/reference/android/app/Notification.Builder.html#setOnlyAlertOnce(boolean)

        # noti.setTicker(j_string(ticker.encode('utf-8')))
        noti.setSmallIcon(icon)
        if self._device.sdk_ver >= 16:
            # for action, name, icon, callback in buttons:
            #     pending_intent = pending_intent.getActivity(service, 0, intent, pending_intent.FLAG_ONE_SHOT)
            #     if self._device.sdk_ver >= 23:
            #         # If you are using PyCharm: Ignore the possible referenced before assignment here, it is not
            #         action = action_builder(icon, j_string(name), pending_intent)
            #         action = action.build()
            #         noti.addAction(action)
            #     else:
            #         noti.addAction(icon, j_string(name), pending_intent)
            for button in self.buttons:
                button_pending_intent = pending_intent.getActivity(service, 1, intent, pending_intent.FLAG_ONE_SHOT)
                if icon not in button:
                    button['icon'] = 17301539
                if self._device.sdk_ver >= 23:
                    # If you are using PyCharm: Ignore the possible referenced before assignment here, it is not
                    action = action_builder(button['icon'], j_string(button['name']), button_pending_intent)
                    action = action.build()
                    noti.addAction(action)
                else:
                    noti.addAction(button['icon'], j_string(button['name']), button_pending_intent)
        if self._device.sdk_ver >= 16:
            noti = noti.build()
        else:
            noti = noti.getNotification()
        noti.color = hex_to_jcolor(colors[self.theme_cls.primary_palette][self.theme_cls.primary_hue])
        self._notification = noti
        self._android_activity.mActivity.getSystemService(context.NOTIFICATION_SERVICE).notify(self._id, noti)
        if self.update_on_theme_color:
            self.theme_cls.bind(primary_color=self._update_theme_color)
        return self

    @platform_required('android')
    def add_button(self, name, icon=None):
        if icon is None:
            button = {'name': name}
        else:
            button = {'name': name, 'icon': icon}
        self.buttons.append(button)

    @platform_required('android')
    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.send()

    @platform_required('android')
    def _update_theme_color(self, *args, **kwargs):
        self.update()

    @platform_required('android')
    def dismiss(self):
        context = autoclass('android.content.Context')
        self._android_activity.mActivity.getSystemService(context.NOTIFICATION_SERVICE).cancel(self._id)

    def on_dismiss(self, notification):
        pass

notification_manager = NotificationManager()

#
#
# @platform_required('android')
#     def android_notify(self, title="", message="", icon="stat_sys_download", buttons=list(), ticker="",
#                        play_sound=False, vibrate=False, pulse_light=False, persistent=False, unique_id=-1,
#                        r_cls="android_device.R$drawable"):
#         """
#         Send a notification.
#
#         :param title: Title of the notification
#         :param message: Message of the notification
#         :param icon: Icon to be displayed along with the message.
#                      If use set r_cls to: "{}.R$drawable".format(self._android_activity.mActivity.getPackageName())
#                      and then set icon to "icon", then it will use the app icon for the notification.
#                      List of standard icons: https://developer.android.com/reference/android/R.drawable.html
#                      I recommend using icons that start with "stat_", as they can be colored, and they look nice
#         :param ticker: text to display on status bar as the notification
#                        arrives (only works on android_device versions less than Lollipop)
#         :param buttons: contains icons and name.
#             :param name: str
#             :param icon: str
#         :param unique_id: The notification id. If you use two notifications with the same id, the one that was created
#                           first will be replaced by the new notification
#         :param r_cls: The android_device.R.drawable class to use, this is where the icon will be pulled from
#         :type title: str
#         :type message: str
#         :type icon: str
#         :type ticker: str
#         :type buttons: dict
#         """
#         # TODO: Pressing a small button while the activity is focused, will throw a Java exception,
#         # but nothing bad happens
#         if unique_id == -1:
#             unique_id = int(self.android_config.android_current_notification_id)
#             self.android_config.android_current_notification_id += 1
#             # We use int() here so that unique id is not changed when we do += 1
#         pending_intent = autoclass('android_device.app.PendingIntent')
#         intent_cls = autoclass('android_device.content.Intent')
#         j_string = autoclass('java.lang.String')
#         notification_builder = autoclass('android_device.app.Notification$Builder')
#         notification_cls = autoclass('android_device.app.Notification')
#         drawable = autoclass(r_cls)
#         context = autoclass('android_device.content.Context')
#         if self._android_device.sdk_ver >= 23:
#             action_builder = autoclass('android_device.app.Notification$Action$Builder')
#
#         service = self._android_activity.mActivity
#         intent = intent_cls(self._android_activity.mActivity, self._android_activity.getClass())
#         intent.setFlags(intent_cls.FLAG_ACTIVITY_CLEAR_TOP | intent_cls.FLAG_ACTIVITY_SINGLE_TOP |
#                         intent_cls.FLAG_ACTIVITY_REORDER_TO_FRONT)
#         try:
#             icon = getattr(drawable, icon)
#         except AttributeError:
#             Logger.warn("Icon \"{}\" was not found in drawable class \"{}\", using backup icon: " +
#                         "android_device.R$drawable.stat_sys_warning".format(icon, r_cls))
#             drawable = autoclass('android_device.R$drawable')
#             icon = drawable.stat_sys_warning
#         noti = notification_builder(self._android_activity.mActivity)
#         noti_flags = list()
#         if play_sound:
#             noti_flags.append(notification_cls.DEFAULT_SOUND)
#         if vibrate:
#             noti_flags.append(notification_cls.DEFAULT_VIBRATE)
#         if pulse_light:
#             noti_flags.append(notification_cls.DEFAULT_LIGHTS)
#         if persistent:
#             noti_flags.append(notification_cls.FLAG_ONGOING_EVENT)
#             noti_flags.append(notification_cls.FLAG_NO_CLEAR)
#             noti.setOngoing(True)
#         else:
#             noti.setAutoCancel(True)
#         noti_flags_int = 0
#         for flag in noti_flags:
#             noti_flags_int |= flag
#         noti.setDefaults(noti_flags_int)
#         noti.setContentIntent(pending_intent.getActivity(service, 0, intent, pending_intent.FLAG_ONE_SHOT))
#
#         noti.setContentTitle(j_string(title.encode('utf-8')))
#         noti.setContentText(j_string(message.encode('utf-8')))
#         noti.setTicker(j_string(ticker.encode('utf-8')))
#         noti.setSmallIcon(icon)
#
#         if self._android_device.sdk_ver >= 16:
#             # for action, name, icon, callback in buttons:
#             #     pending_intent = pending_intent.getActivity(service, 0, intent, pending_intent.FLAG_ONE_SHOT)
#             #     if self._android_device.sdk_ver >= 23:
#             #         # If you are using PyCharm: Ignore the possible referenced before assignment here, it is not
#             #         action = action_builder(icon, j_string(name), pending_intent)
#             #         action = action.build()
#             #         noti.addAction(action)
#             #     else:
#             #         noti.addAction(icon, j_string(name), pending_intent)
#             for button in buttons:
#                 if icon not in button:
#                     button['icon'] = 17301539
#                 pending_intent = pending_intent.getActivity(service, 0, intent, pending_intent.FLAG_ONE_SHOT)
#                 if self._android_device.sdk_ver >= 23:
#                     # If you are using PyCharm: Ignore the possible referenced before assignment here, it is not
#                     action = action_builder(button['icon'], j_string(button['name']), pending_intent)
#                     action = action.build()
#                     noti.addAction(action)
#                 else:
#                     noti.addAction(button['icon'], j_string(button['name']), pending_intent)
#
#         if self._android_device.sdk_ver >= 16:
#             noti = noti.build()
#         else:
#             noti = noti.getNotification()
#         noti.color = hex_to_jcolor(colors[self.theme_cls.primary_palette][self.theme_cls.primary_hue])
#         self._android_activity.mActivity.getSystemService(context.NOTIFICATION_SERVICE).notify(unique_id, noti)
#         return AndroidNotificationWrapper(self, noti, unique_id)

# notification_button1 = {"action": "speak",
#                         "name": "speak"}
# # button_action = "speak"
# # button_name = "speak"
# # button_icon = 17301539
# # button_callback = self.noti_callback
# buttons = list()
# buttons.append(notification_button1)
# self.android_notify(buttons=buttons, title="hello world", message="this is the message",
#                     play_sound=True, pulse_light=True, vibrate=True)
