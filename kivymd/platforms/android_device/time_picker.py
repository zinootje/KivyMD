import datetime
from functools import partial

from kivy.event import EventDispatcher
from kivy.properties import ObjectProperty, NumericProperty, BooleanProperty

from kivymd.app import MDApp
from kivymd.platforms.android_device import PythonJavaClass, java_method, autoclass
from kivymd.platforms.decorators import platform_required, run_on_ui_thread
from kivymd.theming import ThemableBehavior


class _OnTimeSetListener(PythonJavaClass):
    __javainterfaces__ = ['android/app/TimePickerDialog$OnTimeSetListener', ]
    __javacontext__ = 'app'

    @platform_required('android')
    def __init__(self, action):
        self.action = action
        super(_OnTimeSetListener, self).__init__()

    @platform_required('android')
    @java_method('(Landroid/widget/TimePicker;II)V')
    def onTimeSet(self, dialog, hour, minute):
        print("BUTTON CLICKED!", dialog, hour, minute)
        dt = datetime.time(hour=hour, minute=minute)
        self.action(dialog, dt)


class AndroidTimePickerDialog(EventDispatcher, ThemableBehavior):
    __events__ = ('on_dismiss', 'on_time_set')
    _time_picker_dialog = ObjectProperty()
    hour = NumericProperty()
    minutes = NumericProperty()
    use_24_hour = BooleanProperty()
    callback = ObjectProperty()

    @platform_required('android')
    def __init__(self, callback, hour=12, minutes=0, use_24_hour=False):
        super(AndroidTimePickerDialog, self).__init__()
        self.callback = callback
        self.hour = hour
        self.minutes = minutes
        self.use_24_hour = use_24_hour
        self._time_picker_java = autoclass('android.app.TimePickerDialog')
        self._context = MDApp.get_running_app()._android_activity.mActivity

    @platform_required('android')
    @run_on_ui_thread
    def open(self):
        r_style = autoclass('android.R$style')
        style = r_style.Theme_Material_Dialog if self.theme_cls.theme_style == "Dark" else\
            r_style.Theme_Material_Light_Dialog

        self._time_picker_dialog = self._time_picker_java(self._context, style,
                                                          _OnTimeSetListener(partial(self.callback)),
                                                          self.hour, self.minutes, self.use_24_hour)

        self._time_picker_dialog.show()

    def on_time_set(self, instance, time):
        pass

    def on_dismiss(self, instance):
        pass

# TODO: Events
