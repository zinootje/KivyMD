from functools import wraps
from kivy import Logger
from kivy import platform
from kivy.app import App

if platform == "android":
    from android.runnable import Runnable
else:
    class Runnable:
        def __init__(self, *args, **kwargs):
            pass

        def __call__(self, *args, **kwargs):
            pass


def platform_required(*required_platform):
    def platform_decorator(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            for plat in required_platform:
                if plat == platform:
                    return func(*args, **kwargs)
            Logger.warn("KivyMD-Android: Platform requirement of [{}] for "
                        "function \"{}\" was not met (is \"{}\")".format(", ".join(required_platform), func.__name__,
                                                                         platform))
            return
        return wrap
    return platform_decorator


def android_api_required(api_level):
    def api_decorator(func):
        @platform_required('android')
        @wraps(func)
        def wrap(*args, **kwargs):
            actual_api = App.get_running_app()._device.sdk_ver
            if actual_api >= api_level:
                return func(*args, **kwargs)
            else:
                Logger.warn("KivyMD-Android: Android API requirement of {} for "
                            "function \"{}\" was not met (is {})".format(api_level, func.__name__, actual_api))
                return
        return wrap
    return api_decorator


def run_on_ui_thread(func):
    @platform_required('android')
    @wraps(func)
    def wrap(*args, **kwargs):
        Runnable(func)(*args, **kwargs)
    return wrap
